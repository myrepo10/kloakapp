package com.myapp.kloakapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KloakappApplication {

	public static void main(String[] args) {
		SpringApplication.run(KloakappApplication.class, args);
	}

}
