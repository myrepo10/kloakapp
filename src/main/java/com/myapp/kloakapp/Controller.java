package com.myapp.kloakapp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping(path = "/")
    public String rootLoc() {
        return "rootLocation";
    }

    @GetMapping("/resources")
    public String resources(){
        return "Some confidential resources are here";
    }
}
